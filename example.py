from simple_salesforce import Salesforce
from salesforce_login_performer import SalesforceLoginPerformer


def validation_callback(verification_message: str) -> str:
    """
    When logging on for the first time, Salesforce will request user verification using email or phone.
    This method is called only when Salesforce requests verification.
    SalesforceLoginPerformer saves cookies after successful login, so subsequent logins should not trigger
    verification, though it cannot be guaranteed.

    This example implementation requests user to enter verification code, which is fine for running scripts
    on your machine or for non critical deployments.
    For critical production systems I'd suggest to implement checking mailbox and returning code sent by Salesforce.


    :param verification_message: verification message from Salesforce.
    For example "Enter the verification code we emailed to ema****@**il.com."

    :return: Verification code sent by Salesforce
    """
    code = input(verification_message)
    return code


def login(login: str, password: str):
    """
    Example login method to use in your code. Performs login and initializes simple_salesforce

    :param login: Salesforce login
    :param password: Salesforce password
    :return: Logged in simple_salesforce lib. Ready to use
    """
    login_performer = SalesforceLoginPerformer(validation_callback)
    login_result = login_performer.login(login, password)

    sf = Salesforce(**login_result)
    return sf


login_sf = 'salesforce_login@gmail.com'
password = 'salesforce_password'

sf = login(login_sf, password)

# Verify that API works. Change this code for yours
res = sf.query("select Id from Account limit 1")
for r in res['records']:
    print(r)
