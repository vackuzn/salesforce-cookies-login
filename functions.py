from lxml import html
from urllib.parse import urlparse


def compose_abs_url(request_url, url_to_compose):
    if url_to_compose[0] == '/':
        url_base = "{uri.scheme}://{uri.netloc}".format(uri=urlparse(request_url))
        return url_base + url_to_compose
    return url_to_compose


def parse_form_submit_data(response, xpath_get_form: str):
    doc = html.fromstring(response.content)
    xpath_result = doc.xpath(xpath_get_form)
    if len(xpath_result) == 0:
        raise KeyError(f"Xpath query '{xpath_get_form}' returned no results")

    form = xpath_result[0]

    action_url = compose_abs_url(response.url, form.action)
    submit_data = {}

    to_post_fields = form.xpath("//input[@name]")
    for field in to_post_fields:
        submit_data[field.name] = field.value if field.value is not None else ''

    return action_url, submit_data
