# Description

Implementation of Salesforce API cookie based login. Idea is to pretend a browser and login to https://login.salesforce.com/ then take **sid** cookie value which is 
a valid session ID for accessing API.

# Requirements
*  Python 3
*  virtualenv recommended

# Project setup
1.  Open terminal on linux or git bash on windows. `cd` into your projects folder
2.  Clone repository. `git clone https://gitlab.com/vackuzn/salesforce-cookies-login.git && cd salesforce-cookies-login`
3.  Create virtual environment in **venv** folder. `virtualenv venv`. You may need to use `--python` argument to specify python version
4.  Activate virtual environment. `source venv/Scripts/activate`
5.  Install requirements. `pip install -r requirements.txt`

# First run
When logging in to Salesforce for the first time, after entering valid credentials Salesforce will issue email or phone verification.
After successful verification, it will not take place in subsequent logins.
Tool saves cookies after successful login to avoid verification on the next login, but verification has to be done for the first time.

Making first run:
1.  Open project folder using your IDE of choice, make sure to set it to use created virtual environment
2.  Edit `example.py` and specify login and password
3.  Run `example.py` in IDE or using command line `python example.py`. If you are using terminal, make sure virtual environment is activated (step 4 in project setup section)
4.  Most likely verification will be requested. Enter code you've received by email or phone
5.  If login completes successfully, you'll see test query result printed on console
6.  Run `example.py` again, verification shouldn't be requested again

Done! Now you can run your API queries.

# Security considerations
`cookies` file gets created in the project folder. It contains all cookies from the requests session, including session id, 
which is still valid for some time (around 3 hours) after last API request was made. As this file contains sensitive information, make sure to secure it properly