import requests
import pickle


class BrowserEmulator:
    def __init__(self, headers: dict = None):
        self._session = requests.Session()
        if headers:
            self._session.headers = {**self._session.headers, **headers}

    def get(self, url: str):
        response = self._session.get(url)
        response.raise_for_status()
        return response

    def post(self, url, data):
        response = self._session.post(url, data)
        response.raise_for_status()
        return response

    def get_cookies(self):
        return self._session.cookies

    def save_cookies(self, file_path: str):
        with open(file_path, 'wb') as f:
            pickle.dump(self._session.cookies, f)

    def load_cookies(self, file_path: str):
        with open(file_path, 'rb') as f:
            self._session.cookies.update(pickle.load(f))
