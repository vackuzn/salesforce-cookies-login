class InvalidCredentialsError(Exception):
    pass


class LoginFailedError(Exception):
    pass
