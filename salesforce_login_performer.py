import re
from typing import Callable
from os import path
from simple_salesforce import Salesforce
from simple_salesforce.exceptions import SalesforceExpiredSession
from browser_emulator import BrowserEmulator
from functions import parse_form_submit_data
from exceptions import LoginFailedError, InvalidCredentialsError


def get_login_url(is_sandbox: bool = False):
    if is_sandbox:
        return "https://test.salesforce.com/"
    return "https://login.salesforce.com/"


def test_salesforce_login(login_result: dict):
    """
    Perform test Salesforce query. If login result is invalid, exception will be thrown
    """
    sf = Salesforce(**login_result)
    sf.query("select Id from Account limit 1")


def invalid_credentials_error(response):
    return "Please check your username and password." in response.text


def fill_credentials(enter_credentials_form_data, login: str, password: str):
    enter_credentials_form_data[1]['un'] = login
    enter_credentials_form_data[1]['pw'] = password

    return enter_credentials_form_data


def create_browser_like_user_agent_headers():
    return {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0',
    }


class SalesforceLoginPerformer:
    def __init__(self,
                 login_verification_handler: Callable[[str], str],
                 cookies_file: str = "cookies",
                 is_sandbox: bool = False):

        self._verification_handler = login_verification_handler
        self._browser = BrowserEmulator(headers=create_browser_like_user_agent_headers())
        self._cookies_file = cookies_file
        self._login_url = get_login_url(is_sandbox)

    def login(self, login: str, password: str, force_relogin: bool = False):
        """
        Performs Salesforce login. If incorrect credentials entered, InvalidCredentialsError is raised.
        If no valid session is found in cookies at the end of the procedure, LoginFailedError is raised.

        :param login: Salesforce login
        :param password: Salesforce password
        :param force_relogin: If set to false and sid cookie found in saved cookies from previous run,
        it will first try to use previous session if still valid (valid for around 3 hours since last activity).
        If you have only one user to login, leave force_relogin = False.
        If multiple user credentials are used, set this force_relogin = True to avoid caching previous login for
        a different user

        :return: Returns dictionary {'session_id': <session_id>, 'instance_url': <instance_url>}
        It can be directly passed to simple_salesforce like this: sf = Salesforce(**login_result)
        """
        if path.exists(self._cookies_file):
            self._browser.load_cookies(self._cookies_file)
            if not force_relogin:
                login_result = self._find_valid_login_result()
                if login_result:
                    return login_result

        return self._perform_login(login, password)

    def _perform_login(self, login: str, password: str):
        # Get https://login.salesforce.com/
        enter_credentials_page_response = self._browser.get(self._login_url)

        # Parse login form and set credentials
        enter_credentials_form_data = parse_form_submit_data(enter_credentials_page_response, "//form[@name='login']")
        fill_credentials(enter_credentials_form_data, login, password)

        # Submit login form
        login_attempt_response = self._browser.post(*enter_credentials_form_data)

        # Check if incorrect credentials error occurred
        if invalid_credentials_error(login_attempt_response):
            raise InvalidCredentialsError()

        # Handle email or phone verification if requested
        self._handle_verification_if_requested(login_attempt_response)

        # Browse cookies and try to find valid session id. If no valid session found, error is raised
        login_result = self._find_valid_login_result()
        if not login_result:
            raise LoginFailedError()

        # At this point login is successful. Save cookies to file to avoid verification on the next login
        self._browser.save_cookies(self._cookies_file)

        return login_result

    def _handle_verification_if_requested(self, response):
        """
        Function is called recursively to handle incorrect code enter and ask again
        """
        match = re.search("Enter the verification code[^<]+", response.text)
        if not match:
            return response

        # Call verification handler to get requested code
        verification_message = match.group(0)
        code = self._verification_handler(verification_message)

        # Parse verification form and enter code
        form_data = parse_form_submit_data(response, '//form')
        form_data[1]['emc'] = code

        # Submit verification form
        verification_response = self._browser.post(*form_data)

        # Recursively call the function. If entered code was correct, function will return at regex.search check.
        # If code was incorrect, another attempt will be made
        return self._handle_verification_if_requested(verification_response)

    def _find_valid_login_result(self):
        login_results = self._get_login_results_from_cookies()
        for lr in login_results:
            try:
                test_salesforce_login(lr)
                return lr
            except SalesforceExpiredSession:
                continue
        return None

    def _get_login_results_from_cookies(self):
        login_results = []
        for cookie in self._browser.get_cookies():
            if cookie.name == "sid" and cookie.domain.endswith(".salesforce.com"):
                login_results.append({'session_id': cookie.value, 'instance_url': "https://" + cookie.domain})

        return login_results
